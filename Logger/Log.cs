﻿using System;
using System.Reflection;
using System.IO;
using System.IO.Pipes;
using System.Runtime.InteropServices;
using static System.Diagnostics.Debug;
using System.Diagnostics;
using System.Text;

using CFP = System.Runtime.CompilerServices.CallerFilePathAttribute;
using CLN = System.Runtime.CompilerServices.CallerLineNumberAttribute;

namespace Logging
{
    public class Logger
    {
        // log entry packet which precedes the strings in the pipe
        private struct LogEntrySender
        {
            public long timestamp;
            public int line_number;
            public int channel_length;
            public int text_length;
            public int filename_length;
        }

        public enum WriteToFile
        {
            Never,
            Always,
            IfNoDebugger
        };

        public WriteToFile WriteToFileOption
        {
            set
            {
                file_option = value;
            }
        }

        WriteToFile file_option = WriteToFile.Never;

        public string Filename = "log.txt";

        static bool append_to_log = false;

        // for receiving log entries from the client
        static NamedPipeClientStream pipe;

        // a packet to receive into
        static LogEntrySender sender = new LogEntrySender();

        // which is yay big
        static int sender_size = Marshal.SizeOf(sender);

        // unmanaged buffer for marshalling
        static IntPtr sender_buffer = Marshal.AllocHGlobal(sender_size);

        // main receive buffer
        static byte[] sender_bytes = new byte[65536];

        // did we already attempt to open the pipe?
        static bool attempted_pipe_open = false;

        // lock to block access from multiple threads
        static object log_window_lock_object = new object();
        static object file_lock_object = new object();

        // the one global logger
        public static Logger Log = new Logger();

        private const string timestamp_export_format_string = "yyyy-MM-dd,hh:mm:ss.fffff";
        private const string QUOTE = "\"";
        private const string ESCAPED_QUOTE = "\"\"";
        private static char[] CHARACTERS_THAT_MUST_BE_QUOTED = { ',', '"', '\n' };

        // private ctor - it's a singleton
        private Logger()
        {
        }

        static string pipe_guid = "8834E49A-9D77-4166-AD66-EF29CBDA2B13";


        [DllImport("Kernel32.dll", SetLastError = true, ExactSpelling = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool CheckRemoteDebuggerPresent(IntPtr hProcess, [MarshalAs(UnmanagedType.Bool)] out bool isDebuggerPresent);

        private static bool IsDebuggerAttached
        {
            get
            {
                return CheckRemoteDebuggerPresent(Process.GetCurrentProcess().Handle, out bool isDebuggerAttached) && isDebuggerAttached;
            }
        }

        // attempt to open the pipe to the extension
        private static bool OpenPipe()
        {
            if (!attempted_pipe_open)
            {
                attempted_pipe_open = true;
                int process_id = Process.GetCurrentProcess().Id;
                WriteLine("Process ID is " + process_id);
                string pipe_name = string.Format("{0:x8}_{1}", process_id, pipe_guid);
                try
                {
                    pipe = new NamedPipeClientStream(".", pipe_name, PipeDirection.Out, PipeOptions.WriteThrough);
                }
                catch (Exception e)
                {
                    WriteLine("Failed to create NamedPipeClient, name = " + pipe_name + "Exception: " + e.Message);
                    pipe = null;
                    return false;
                }
                try
                {
                    pipe.Connect(1000);
                }
                catch (Exception e)
                {
                    WriteLine("Failed to connect NamedPipeClient, name = " + pipe_name + "Exception: " + e.Message); WriteLine(e.Message);
                    pipe = null;
                }
            }
            return pipe != null;
        }

        private static string Escape(string s)
        {
            if (s.Contains(QUOTE))
                s = s.Replace(QUOTE, ESCAPED_QUOTE);

            if (s.IndexOfAny(CHARACTERS_THAT_MUST_BE_QUOTED) > -1)
                s = QUOTE + s + QUOTE;

            return s;
        }

        // Write a log entry
        public void Write(string channel, object text, [CFP] string filePath = "", [CLN] int lineNumber = 0)
        {
            DateTime now = DateTime.Now;
            bool being_debugged = IsDebuggerAttached;

            // write to log file, maybe
            if (file_option == WriteToFile.Always || file_option == WriteToFile.IfNoDebugger && !being_debugged)
            {
                lock (file_lock_object)
                {
                    try
                    {
                        using (StreamWriter stream = new StreamWriter(Filename, append_to_log))
                        {
                            append_to_log = true;
                            string line = string.Format("{0},{1},{2},{3},{4}", now.ToString(timestamp_export_format_string),
                                                                               Escape(channel),
                                                                               Escape(text.ToString()),
                                                                               Escape(filePath),
                                                                               lineNumber);
                            stream.WriteLine(line);
                            stream.Flush();
                            stream.Close();
                        }
                    }
                    catch (IOException ex)
                    {
                        WriteLine($"Can't write Log Message to {Filename} ({ex.Message}), switching off file logging.");
                        file_option = WriteToFile.Never;
                    }
                }
            }

            // not LogWindow if not running in the debugger
            if (being_debugged)
            {
                // one at a time, gents
                lock (log_window_lock_object)
                {
                    // fire up the pipe
                    if (OpenPipe())
                    {
                        string text_text = text.ToString();

                        // create a blob of descriptor header
                        sender.timestamp = now.ToFileTime();
                        sender.line_number = lineNumber;
                        sender.channel_length = channel.Length;
                        sender.text_length = text_text.Length;
                        sender.filename_length = filePath.Length;

                        Marshal.StructureToPtr(sender, sender_buffer, true);

                        // assemble the message
                        Marshal.Copy(sender_buffer, sender_bytes, 0, sender_size);
                        int t1 = Encoding.UTF8.GetBytes(channel, 0, channel.Length, sender_bytes, sender_size);
                        int t2 = Encoding.UTF8.GetBytes(text_text, 0, text_text.Length, sender_bytes, sender_size + t1);
                        int t3 = Encoding.UTF8.GetBytes(filePath, 0, filePath.Length, sender_bytes, sender_size + t1 + t2);

                        // send it
                        pipe.Write(sender_bytes, 0, sender_size + t1 + t2 + t3);
                    }
                }
            }
        }
    }
}

// handy extensions for some common channels, add more as you wish, here or wherever
public static class LoggingExtensions
{
    public static void Debug(this Logging.Logger Log, object text, [CFP] string f = "", [CLN] int l = 0)
    {
        Log.Write("Debug", text, f, l);
    }

    public static void Warning(this Logging.Logger Log, object text, [CFP] string f = "", [CLN] int l = 0)
    {
        Log.Write("Warning", text, f, l);
    }

    public static void Info(this Logging.Logger Log, object text, [CFP] string f = "", [CLN] int l = 0)
    {
        Log.Write("Info", text, f, l);
    }

    public static void Error(this Logging.Logger Log, object text, [CFP] string f = "", [CLN] int l = 0)
    {
        Log.Write("Error", text, f, l);
    }

    public static void Exception(this Logging.Logger Log, object text, [CFP] string f = "", [CLN] int l = 0)
    {
        Log.Write("Exception", text, f, l);
    }
}

