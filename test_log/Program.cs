﻿using System;
using System.Runtime.CompilerServices;
using System.Threading;

using static Logging.Logger;

namespace test_log
{
    class Program
    {
        static void Main(string[] args)
        {
            Log.WriteToFileOption = WriteToFile.IfNoDebugger;
            Log.Filename = "log.txt";

            Thread.Sleep(2000);

            Guid g = Guid.NewGuid();

            Log.Debug("Some debug info");
            Log.Info("Some more info");
            Log.Error($"It's an error: {g}");
            Log.Foo("The foo is in flight");
            for(int i=0; i<50; ++i)
            {
                new Thread((id) => {
                    Thread.Sleep(5001 - i * 100);
                    Log.Write("Thread" + id, "Thread is running: " + id);
                }).Start(i);
            }
        }
    }

    public static class Ext
    {
        public static void Foo(this Logging.Logger Log, object text, [CallerFilePath] string f = "", [CallerLineNumber] int l = 0)
        {
            Log.Write("Foo", text, f, l);
        }
    }
}
