﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

using static Logging.Logger;

namespace Logging
{
    public static class Log
    {
        public static void V(string tag, object text, [CallerFilePath] string filepath = "", [CallerLineNumber] int line = 0)
        {
        }
        public static void D(string tag, object text, [CallerFilePath] string filepath = "", [CallerLineNumber] int line = 0)
        {
        }
        public static void I(string tag, object text, [CallerFilePath] string filepath = "", [CallerLineNumber] int line = 0)
        {
        }
        public static void W(string tag, object text, [CallerFilePath] string filepath = "", [CallerLineNumber] int line = 0)
        {
        }
        public static void E(string tag, object text, [CallerFilePath] string filepath = "", [CallerLineNumber] int line = 0)
        {
        }
    }

    public class Test
    {
        const string TAG = "Test";

        public Test()
        {
            Log.D(TAG, "Hello");
        }
    }
}
